#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  7 08:52:38 2020

@author: mac
"""

import tensorflow_hub as hub
import tensorflow as tf

import numpy as np
import os
import urllib

import pandas as pd
from baselines import one_hot, empirical_dist
from deep_learning import make_mlp, DenseTransformer
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from keras.wrappers.scikit_learn import KerasClassifier
from serialization import save_pipeline, load_pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split 
from sklearn import metrics



import spacy
from tqdm import tqdm
import re
import time
import pickle
pd.set_option('display.max_colwidth', 200)




DATA_URL =''
MODEL_URL =''
# Figshare URLs for downloading training data
ATTACK_ANNOTATED_COMMENTS_URL = 'https://ndownloader.figshare.com/files/7554634'
ATTACK_ANNOTATIONS_URL = 'https://ndownloader.figshare.com/files/7554637'
AGGRESSION_ANNOTATED_COMMENTS_URL = 'https://ndownloader.figshare.com/files/7038038'
AGGRESSION_ANNOTATIONS_URL = 'https://ndownloader.figshare.com/files/7383748'
TOXICITY_ANNOTATED_COMMENTS_URL = 'https://ndownloader.figshare.com/files/7394542'
TOXICITY_ANNOTATIONS_URL = 'https://ndownloader.figshare.com/files/7394539'
# CSV of optimal  hyper-parameters for each model architecture
CV_RESULTS = 'cv_results.csv'

def download_file(url, fname):
    """
    Helper function for downloading a file to disk
    """
    urllib.request.urlretrieve(url, fname)
    
def download_training_data(data_dir, task):

    """
    Downloads comments and labels for task
    """

    COMMENTS_FILE = "%s_annotated_comments.tsv" % task
    LABELS_FILE = "%s_annotations.tsv" % task

    if task == "attack":
        download_file(ATTACK_ANNOTATED_COMMENTS_URL,
                      os.path.join(data_dir, COMMENTS_FILE))
        download_file(ATTACK_ANNOTATIONS_URL, os.path.join(data_dir,
                      LABELS_FILE))
    elif task == "recipient_attack":
        download_file(ATTACK_ANNOTATED_COMMENTS_URL,
                      os.path.join(data_dir, COMMENTS_FILE))
        download_file(ATTACK_ANNOTATIONS_URL, os.path.join(data_dir,
                      LABELS_FILE))
    elif task == "aggression":
        download_file(AGGRESSION_ANNOTATED_COMMENTS_URL,
                      os.path.join(data_dir, COMMENTS_FILE))
        download_file(AGGRESSION_ANNOTATIONS_URL,
                      os.path.join(data_dir, LABELS_FILE))
    elif task == "toxicity":
        download_file(TOXICITY_ANNOTATED_COMMENTS_URL,
                      os.path.join(data_dir, COMMENTS_FILE))
        download_file(TOXICITY_ANNOTATIONS_URL,
                      os.path.join(data_dir, LABELS_FILE))
    else:
        print("No training data for task: ", task)
        
        
def parse_training_data(data_dir, task):

    """
    Computes labels from annotations and aligns comments and labels for training
    """

    COMMENTS_FILE = "%s_annotated_comments.tsv" % task
    LABELS_FILE = "%s_annotations.tsv" % task

    comments = pd.read_csv(os.path.join(data_dir, COMMENTS_FILE), sep = '\t', index_col = 0)
    # remove special newline and tab tokens

    comments['comment'] = comments['comment'].apply(lambda x: x.replace("NEWLINE_TOKEN", " "))
    comments['comment'] = comments['comment'].apply(lambda x: x.replace("TAB_TOKEN", " "))


    annotations = pd.read_csv(os.path.join(data_dir, LABELS_FILE),  sep = '\t', index_col = 0)
    labels = empirical_dist(annotations[task])

    X = comments.sort_index()['comment'].values
    y = labels.sort_index().values

    assert(X.shape[0] == y.shape[0])
    return X, y


task ='attack'

print("Downloading Data")
#download_training_data(DATA_URL, task)

print("Parsing Data")
X, y = parse_training_data(DATA_URL, task)
y = np.argmax(y, axis = 1)
dataset=np.concatenate((X.reshape(-1,1),y.reshape(-1,1)),axis=1)


df = pd.DataFrame(data=dataset, columns=['X','y'])
#X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

trac = pd.read_csv("annotated.csv")
x_trac=trac["Text"].to_numpy()
y_trac= trac["attack"].to_numpy()


# remove URL's from train and test
df['clean'] = df['X'].apply(lambda x: re.sub(r'http\S+', '', x))
trac["clean"]=trac["Text"].apply(lambda x: re.sub(r'http\S+', '', x))


# remove punctuation marks
punctuation = '!"#$%&()*+-/:;<=>?@[\\]^_`{|}~'

df['clean'] = df['clean'].apply(lambda x: ''.join(ch for ch in x if ch not in set(punctuation)))
trac["clean"]=trac["clean"].apply(lambda x: ''.join(ch for ch in x if ch not in set(punctuation)))



# convert text to lowercase
df['clean'] = df['clean'].str.lower()
trac["clean"]=trac["clean"].str.lower()

# remove numbers
df['clean'] = df['clean'].str.replace("[0-9]", " ")
trac["clean"]=trac["clean"].str.replace("[0-9]", " ")

# remove whitespaces
df['clean'] = df['clean'].apply(lambda x:' '.join(x.split()))
trac["clean"]=trac["clean"].apply(lambda x:' '.join(x.split()))
# import spaCy's language model
nlp = spacy.load('en', disable=['parser', 'ner'])

# function to lemmatize text
def lemmatization(texts):
    output = []
    for i in texts:
        s = [token.lemma_ for token in nlp(i)]
        output.append(' '.join(s))
    return output


df['clean'] = lemmatization(df['clean'])
trac['clean'] = lemmatization(trac['clean'])



df.sample(10)







elmo = hub.Module("https://tfhub.dev/google/elmo/2", trainable=True)

# just a random sentence
x = ["Roasted ants are a popular snack in Columbia"]

# Extract ELMo features 
embeddings = elmo(x, signature="default", as_dict=True)["elmo"]

embeddings.shape


def elmo_vectors(x):
  embeddings = elmo(x.tolist(), signature="default", as_dict=True)["elmo"]

  with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    sess.run(tf.tables_initializer())
    # return average of ELMo features
    return sess.run(tf.reduce_mean(embeddings,1))

list_df = [df[i:i+100] for i in range(0,df.shape[0],100)]
list_trac = [trac[i:i+100] for i in range(0,trac.shape[0],100)]


# Extract ELMo embeddings
elmo_df = [elmo_vectors(x['clean']) for x in list_df]
elmo_trac = [elmo_vectors(x['clean']) for x in list_trac]


elmo_df_new = np.concatenate(elmo_df, axis = 0)
elmo_trac_new = np.concatenate(elmo_trac, axis = 0)

# save elmo_train_new
pickle_out = open("elmo_train_03032019.pickle","wb")
pickle.dump(elmo_df_new, pickle_out)
pickle_out.close()

pickle_out = open("elmo_train_03032019.pickle","wb")
pickle.dump(elmo_trac_new, pickle_out)
pickle_out.close()


pickle_in = open("elmo_train_03032019.pickle", "rb")
elmo_train_new = pickle.load(pickle_in)

pickle_in = open("elmo_train_03032019.pickle", "rb")
elmo_test_new = pickle.load(pickle_in)


xtrain, xvalid, ytrain, yvalid = train_test_split(elmo_train_new, 
                                                  df['label'],  
                                                  random_state=42, 
                                                  test_size=0.2)


from sklearn.metrics import f1_score

lreg = LogisticRegression()
lreg.fit(xtrain, ytrain)

preds_valid = lreg.predict(xvalid)
f1_score(yvalid, preds_valid)


preds_test = lreg.predict(elmo_test_new)



# prepare submission dataframe
sub = pd.DataFrame({'id':trac["attack"], 'label':preds_test})

# write predictions to a CSV file
sub.to_csv("sub_lreg.csv", index=False)

